<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <script src="../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 style="color:purple" >Email - Edit form</h2>

    <form>
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" placeholder="Enter Email">
        </div>
        <div class="form-group">
            <label for="passward">Email:</label>
            <input type="passward" class="form-control" id="passward" placeholder="Enter Passward">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="edit" class="btn btn-primary">Add</button>
        <button type="save" class="btn btn-primary">Save</button>
        <button type="reset" class="btn btn-primary">Reset</button>
        <button type="back list" class="btn btn-primary">Back List</button>
    </form>
</div>

</body>
</html>