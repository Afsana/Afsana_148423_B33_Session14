<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <script src="../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 style="color:purple" >Gender - Edit form</h2>

    <form>
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="name" class="form-control" id="name" placeholder="Enter Name">
        </div>
        <div class="from-group">
            <label for="Gender">Gender:</label>
            <input type="text" class="form-control" id="gender" placeholder="Enter Gender">
        </div>
        <div class="radio-inline">
            <label><input type="checkbox">Male</label>
            <label><input type="checkbox">Female</label>
        </div>
        <div></div>
        <button type="submit" class="btn btn-danger">Submit</button>
    </form>
</div>

</body>
</html>