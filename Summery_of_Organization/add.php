<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <script src="../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 style="color:purple" >Summary_of_Organization - Add / Create form</h2>

    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="name" class="form-control" id="name" placeholder="Enter name">
        </div>
        <div class="form-group">
            <label for="organization_name">Organization_Name:</label>
            <input type="organization_name" class="form-control" id="organization_name" placeholder="Enter organization_name">
        </div>
        <div></div>
            <button type="add" class="btn btn-warning">Add</button>
            <button type="update" class="btn btn-warning">Update</button>
            <button type="save" class="btn btn-warning">Save</button>
            <button type="reset" class="btn btn-warning">Reset</button>
            <button type="back list" class="btn btn-warning">Back List</button>
    </form>
</div>

</body>
</html>