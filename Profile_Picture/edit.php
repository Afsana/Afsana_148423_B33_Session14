<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <script src="../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 style="color:purple" >Profile_Picture - Edit form</h2>

    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="name" class="form-control" id="name" placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="profile_picture">Profile_Picture:</label>
            <input type="profile_picture" class="form-control" id="profile_picture" placeholder="Enter Profile_Picture">
        </div>
        <div></div>
        <button type="edit pic" class="btn btn-info">Add</button>
        <button type="save pic" class="btn btn-info">Save</button>
        <button type="reset" class="btn btn-info">Reset</button>
        <button type="back list" class="btn btn-info">Back List</button>
    </form>
</div>

</body>
</html>