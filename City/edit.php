<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <script src="../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 style="color:purple" >City - Edit form</h2>

    <form>
        <div class="form-group">
            <label for="city_name">City_Name:</label>
            <input type="city_name" class="form-control" id="city_name" placeholder="Enter city_name">
        </div>
        <div class="form-group">
            <label for="city">City:</label>
            <select>
                <option value="Dhaka">Dhaka</option>
                <option value="Chittagong">Chittagong</option>
                <option value="Rajshahi">Rajshahi</option>
                <option value="Comilla">Comilla</option>
                <option value="Barishal">Barishal</option>
                <option value="Sylhet">Sylhet</option>
                <option value="Dinajpur">Commilla</option>
            </select>
            <div></div>
        <button type="edit" class="btn btn-info">Add</button>
        <button type="save" class="btn btn-info">Save</button>
        <button type="reset" class="btn btn-info">Reset</button>
        <button type="back list" class="btn btn-info">Back List</button>
    </form>
</div>

</body>
</html>