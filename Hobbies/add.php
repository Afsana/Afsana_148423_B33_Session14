<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <script src="../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2 style="color:purple" >Hobbies - Add / Create form</h2>

    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="name" class="form-control" id="name" placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="hobbies">Hobbies:</label>
            <input type="hobbies" class="form-control" id="hobbies" placeholder="Enter Hobbies">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Gerdening</label>
            <label><input type="checkbox"> Music</label>
            <label><input type="checkbox"> Painting</label>
            <label><input type="checkbox"> Collecting Coin</label>
            <label><input type="checkbox"> Playing Games</label>
            <label><input type="checkbox"> Others</label>
        </div>
        <button type="add" class="btn btn-success">Add</button>
        <button type="update" class="btn btn-success">Update</button>
        <button type="save" class="btn btn-success">Save</button>
        <button type="reset" class="btn btn-success">Reset</button>
        <button type="back list" class="btn btn-success">Back List</button>
    </form>
</div>

</body>
</html>