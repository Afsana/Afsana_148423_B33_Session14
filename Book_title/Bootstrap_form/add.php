<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2 style="color:purple" >Book_Title - Add / Create form</h2>
    <form>
        <div class="form-group">
            <label for="book_name">Book_Name:</label>
            <input type="book_name" class="form-control" id="book_name" placeholder="Enter book_name">
        </div>
        <div class="form-group">
            <label for="author_name">Author_Name:</label>
            <input type="author_name" class="form-control" id="author_name" placeholder="Enter author_name">
        </div>
        <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>
        <button type="add" class="btn btn-info">Add</button>
        <button type="update" class="btn btn-info">Update</button>
        <button type="save" class="btn btn-info">Save</button>
        <button type="reset" class="btn btn-info">Reset</button>
        <button type="back list" class="btn btn-info">Back List</button>
    </form>
</div>
</body>
</html>